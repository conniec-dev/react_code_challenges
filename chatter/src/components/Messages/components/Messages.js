import React, { useContext, useEffect } from 'react';
import io from 'socket.io-client';
import useSound from 'use-sound';
import config from '../../../config';
import LatestMessagesContext from '../../../contexts/LatestMessages/LatestMessages';
import TypingMessage from './TypingMessage';
import Header from './Header';
import Footer from './Footer';
import Message from './Message';
import '../styles/_messages.scss';
import initialBottyMessage from "/Users/conniecanelon/dev/react_code_challenges/chatter/src/common/constants/initialBottyMessage.js"

const socket = io(
  config.BOT_SERVER_ENDPOINT,
  { transports: ['websocket', 'polling', 'flashsocket'] }
);

function Messages() {
  const [ message, setMessage ] = React.useState("");
  const [ messageList, setMessageList ] = React.useState([
    {"id":"1", "user": "tt", "message": initialBottyMessage},
    {"id":"1", "user": "tt", "message": "hello"},
    {"id":"1", "user": "tt", "message": "hello"},
    {"id":"1", "user": "tt", "message": "hello"},
    {"id":"1", "user": "tt", "message": "hello"}
  ])


  const [playSend] = useSound(config.SEND_AUDIO_URL);
  const [playReceive] = useSound(config.RECEIVE_AUDIO_URL);
  const { setLatestMessage } = useContext(LatestMessagesContext);
  

  const sendMessage = () => {
    console.log("message: ", message)
    const newArray = [
      ...messageList,
      {"id": 2, "user": "tt", "message": message}
    ]
    setMessageList(newArray)
    setMessage("")
  };

  const onChangeMessage = (event) => {
    setMessage(event.target.value)
  };

  return (
    <div className="messages">
      <Header />
      <div className="messages__list" id="message-list">
        { messageList.map((e, i) => (
          <Message nextMessage={"hi"} message={e} botTyping={true}/>
        ))}
        
      
      </div>
      <Footer message={message} sendMessage={sendMessage} onChangeMessage={onChangeMessage} />
    </div>
  );
}

export default Messages;
